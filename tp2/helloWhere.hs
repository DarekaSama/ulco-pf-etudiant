borneEtFormatel :: Double -> String
borneEtFormatel nb = show nb ++ " -> " ++ (show format)
    where format = if nb < 0 then 0 else if nb > 1 then 1 else nb

main :: IO()
main = do
    print $ borneEtFormatel 10
    print $ borneEtFormatel (-10)
    print $ borneEtFormatel 0.2
