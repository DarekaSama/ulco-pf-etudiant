main :: IO()
main = do
    putStrLn "Entrez votre valeur : "
    valeur <- getLine
    let val = (read valeur :: Int)
    print (fibo val)

fibo :: Int -> Int
fibo valeur = case valeur of
        0 -> 0
        1 -> 1
        otherwise -> fibo (valeur - 1) + fibo (valeur-2)
    
fib :: Int -> Int
fib 0 = 0
fib 1 = 1
fib n = fib (n-1) + fib (n-2)