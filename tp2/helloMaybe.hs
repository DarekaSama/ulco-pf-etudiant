main :: IO()
main = do
    putStrLn "Entrez votre valeur : "
    valeur <- getLine
    let val = (read valeur :: Double)
    print (readMaybe val)

readMaybe :: Double -> Maybe Double
readMaybe valeur
    | valeur < 0 = Nothing
    | valeur > 0 = Just (valeur + 1)
    | otherwise = Nothing
