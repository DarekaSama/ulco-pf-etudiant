safehead :: [a] -> Maybe a
safehead [] = Nothing
safehead [x] = Just x
safehead (x:xs) = Just x

safeTail :: [a] -> Maybe [a]
safeTail [] = Nothing
safeTail (x:xs) = Just xs

main :: IO ()
main = do
    putStrLn "Entrez votre mot (SAFE) : "
    texte <- getLine
    print (safehead texte)
    print (safeTail texte)
    -- Calcul de norme