main :: IO ()
main = do
    putStrLn "Entrez votre mot (palindrome) : "
    texte <- getLine
    print (palindrome texte)

palindrome :: Eq text => [text] -> Bool
palindrome [] = True
palindrome [_] = True
palindrome text = (head text == last text) && (palindrome (tail (init text)))