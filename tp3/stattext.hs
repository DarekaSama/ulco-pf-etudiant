import System.IO
import Control.Monad

getCounts :: String -> (Int, Int, Int)
getCounts input = (charCount, wordCount, lineCount)
    where charCount = length input
        wordCount = (length . words) input
        lineCount = (length . lines) input

countText :: (Int, Int, Int) -> String
countText (cc,wc,lc) = unwords ["chars : ", show cc, " words : ", show wc, " lines : ", show lc]

main :: IO ()
main = do  
    args <- getArgs
    let filename = head Args
    input <- readFile filename -- "./tp3/test.txt"
    let summary = (countText . getCounts) input
    appendFile "stats.dat" (mconcat [filename, " ", summary, "\n"])
    putStrLn summary

-- A verifier