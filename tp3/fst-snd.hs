myFst :: (a,b) ->  a
myFst (x:y) =  x

mySnd :: (a,b) -> [a]
mySnd (x:xs) = xs

myFst3 :: (a,b,c) ->  a
myFst3 (x:y:xs) = x

main :: IO ()
main = do
    print (myFst ["foobar", 1])
    print (myFst3 ["foobar", 1, 'a'])
    print (mySnd ["foobar", 1])

-- A verifier
