main :: IO ()
main = do
    print (fuzzylength [0])
    print (fuzzylength [0,1])
    print (fuzzylength [0..5])
    print (fuzzylength [])

fuzzylength :: [x] -> String
fuzzylength [] = "empty"
fuzzylength [_] = "one"
fuzzylength [_,_] = "two"
fuzzylength _ = "many"
