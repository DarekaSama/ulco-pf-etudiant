module Main where
import System.Random
main :: IO ()
main = do putStrLn "Trouve la valeur secrete ([1-100])..."
          secret <- randomRIO (1, 100)
          party <- 0
          play secret
 
 
play :: Int -> IO ()
play secret = do guesses <- playGame secret 0
                if party == 1 then
                    do putStrLn $ "Victoire ! Tu as pris " ++ show guesses ++ " essais!"
                else do putStrLn $ "Tu as perdu ! Tu as dépassé les 10 essais  (" ++ show guesses ++ " essais)"
 
playGame :: Int -> Int -> IO Int
playGame secret guesses = do putStr "? "
                             input <- getLine
                             let guess = read input :: Int
                             if guesses > 10 then
                                 do return (party - 1)
                             if guess == secret then
                                do return (guesses + 1)
                             else if guess < secret then
                                do putStrLn "Plus grand !"
                                   playGame secret (guesses + 1)
                             else do putStrLn "Plus petit !"
                                     playGame secret (guesses + 1)   