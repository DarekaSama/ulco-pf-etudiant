import Control.Monad
main = do
    let loop = do 
        putStrLn "Entrez votre nom : "
        nom <- getLine
        let prefix = "Bonjour "
        putStrLn (prefix ++ nom ++ " !")
        when (nom /= "") loop
    loop
    putStrLn ("Au revoir")