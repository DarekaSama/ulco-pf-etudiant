main :: IO ()
main = do
    print (mapDoubler [1..5])
    print (mapDoubler2 [1..5])
    print (filterEven1 [1..5])

mapDoubler :: Num a => [a] -> [a]
mapDoubler = map (*2)

mapDoubler2 :: Num a => [a] -> [a]
mapDoubler2 [] = []
mapDoubler2 (x:xs) = (x*2):(mapDoubler2 xs)

filterEven1 :: Num a => [a] -> [a]
filterEven1 [] = []
filterEven1 (x:xs) = if x `mod` 2 == 0 
    then filterEven1 xs