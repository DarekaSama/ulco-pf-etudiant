main :: IO ()
main = do
    putStrLn "My Range exercice :"
    print (myRangeTuple1 0 9)
    print (myRangeTuple2 9)
    print (myRange' 0 9)

myRangeTuple1 :: Int -> Int  -> [Int]
myRangeTuple1 x y = [x..y]

myRangeTuple2 :: Int -> [Int]
myRangeTuple2 x = myRangeTuple1 0 x

-- Equivalent a myRangeTuple1
myRange' :: Int -> Int -> [Int]
myRange' x y = myRangeRec x cnt
                  where
                    cnt = y - x
                    myRangeRec x 0 = [x]
                    myRangeRec x cnt = x : (myRangeRec (x+1) (cnt-1))

--myCurry :: ((a,b) -> c) -> (a -> b -> c)