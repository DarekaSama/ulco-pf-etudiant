{-# LANGUAGE OverloadedStrings #-}

import qualified GI.Gtk as Gtk
import Data.Text

main :: IO ()
main = do
    _ <- Gtk.init Nothing
    window <- Gtk.windowNew Gtk.WindowTypeToplevel 
    Gtk.windowSetDefaultSize window 300 100
    Gtk.windowSetTitle window "Hello World!"
    _ <- Gtk.onWidgetDestroy window Gtk.mainQuit

    label <- Gtk.labelNew (Just "Hello World!")
    Gtk.containerAdd window label

    box <- Gtk.boxNew Gtk.OrientationVertical 0
    Gtk.containerAdd window box

    entry <- Gtk.entryNew
    Gtk.containerAdd box entry

    buttonAdd <- Gtk.buttonNewWithLabel "Add"
    -- _ <- Gtk.onButtonClicked buttonAdd (func)

    -- func :: 
    Gtk.widgetSetHalign label Gtk.AlignStart

    Gtk.labelSetText label (text0 <> "\n -" <> text1)

    Gtk.widgetShowAll window
    Gtk.main
