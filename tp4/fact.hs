main :: IO ()
main = do
    print(factorielle(1))
    print(factorielle(2))
    print(factorielle(5))
    print(factorielle(6))

factorielle :: Int -> Int
factorielle n =
    if n == 1
        then 1
    else n * factorielle (n-1)