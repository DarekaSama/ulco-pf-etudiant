main = loopEcho 2

loopEcho :: Int -> IO String
loopEcho 0 = return ""
loopEcho n = do
    putStrLn "Entrez votre mot (LoopEcho) : "
    texte <- getLine
    if null texte then do
        putStrLn "Empty line"
    else 
        putStrLn texte
    loopEcho (n -1)