import Data.Text
main :: IO ()
main = do
    putStrLn "Entrez votre mot (Exercice 1) : "
    args <- getLine
    print args
    print (mylength [args])
    print (toUpperString args)


mylength :: [String] ->  Int
mylength [] = 0
mylength [x] = length x

toUpperString :: String ->  [Char]
toUpperString = (Data.Char.toUpper x):xs | (x:xs)
