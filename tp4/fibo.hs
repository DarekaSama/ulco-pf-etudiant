import System.Environment
main :: IO ()
main = do 
    args <- getArgs
    print args
    case args of
        ["naive",nStr] -> print $ fiboNaive $ read nStr
        ["tco",nStr] -> print $ fiboTco $ read nStr
        _ -> putStrLn "usage : < naive | tco> <n>"
    print (fiboNaive 12)
    print (fiboTco 12)

fiboNaive :: Int -> Int
fiboNaive 0 = 0
fiboNaive 1 = 1
fiboNaive n = fiboNaive (n-1) + fiboNaive (n-2)


fiboTco :: Int -> Int
fiboTco 0 = 0
fiboTco 1 = 1
fiboTco n = sum [n, n-1 .. 0]
nCr n r = nn + rr
    where
        nn = sum [n, n-1 .. 0]
        rr = fiboTco r