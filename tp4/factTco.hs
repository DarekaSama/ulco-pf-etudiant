main :: IO ()
main = do
    print(factorielleTerminale(1))
    print(factorielleTerminale(2))
    print(factorielleTerminale(5))
    print(factorielleTerminale(6))

factorielleTerminale :: Int -> Int
factorielleTerminale n = product [n, n-1 .. 1] -- 6 * 5 * 4 * 3 * 2 * 1
nCr n r = nn `div` rr                          -- plage de ce que l'on souhaite
    where
        nn = product [n, n-1 .. n-r+1]
        rr = factorielleTerminale r