main :: IO ()
main = do
    print(pgcd 49 35)

pgcd :: Int -> Int -> Int
pgcd 0 a = a
pgcd a 0 = a
pgcd a b = pgcd c (mod d c)
    where 
        d = max a b
        c = min a b