-- import Text.Read
-- import System.Environment
-- import System.IO


-- type Task = (Int, Bool, String)     -- id, done?, label
-- type Model = (Int, [Task])          -- next id, tasks

-- test :: Model -> Model
-- test (nextI, tasks) = (nextI, reverse tasks)

-- run :: Model -> IO Model
-- run model@(nextId, tasks) = do
--     line <- getLine
--     let wline = words line
--     print wline
--     -- Words
--     case wline of
--         ["print"] -> do
--             print "PRINT"
--             print (head tasks)
--             print (tasks !! 1)
--             let aa = foldr (\ x todo -> show x ++ " " ++  todo) tasks
--             -- filter (\ x -> tasks )
--             run model
--         ["printTodo"] -> do
--             print "todo"
--             print (head tasks)
--             print (tasks)
--             run model
--         ["printDone"] -> do
--             print "done"
--             print (head tasks)
--             print (tasks)
--             run model
--         _ -> do
--             print "nio"
--             run model
--     -- readline
--     -- case of line
--     -- case : print
--         -- (print, print todo, print done)
--     -- case : Add
--         -- (string)
--     -- case : Remove
--         -- (int)
--     -- case : Marks
--         -- (do (int), undo (int))
--     -- case : End
--         -- (exit)

-- -- showTask :: Model -> String
-- -- showTask task = (show $ context task) ++ ": " ++ (text task)


-- main :: IO ()
-- main = do
--     print "########## TODOLIST ##########"
--     args <- getArgs
--     case args of 
--         [filename] -> do
--             contents <- readFile filename
--             let model1 = read contents
--             model2 <- run model1
--             writeFile filename $! show model2
--         _ -> putStr "usage : <filename>"
        
--     -- contents <- readFile "../tasks.txt"
--     -- let model1 = read contents
--     --     model2 = test $! model1
--     -- print model2
--     -- showTask model2
--     -- mapM_ print (snd model2)

import Text.Read
import System.Environment
import System.IO

type Task = (Int, Bool, String)     -- id, done?, description
type Todo = (Int, [Task])           -- nextId, tasks

usage :: IO ()
usage = do
    putStrLn ("usage:")
    putStrLn (" print")
    putStrLn (" print todo")
    putStrLn (" print done")
    putStrLn (" add <string>")
    putStrLn (" do <int>")
    putStrLn (" undo <int>")
    putStrLn (" del <int>")
    putStrLn (" exit")

displayAll :: Todo -> IO ()
displayAll todo = print todo

display :: Task -> IO ()
display (id, done, txt)
  | done = do
    putStrLn $ "[X] " ++ (show id) ++ ". " ++ txt
  | otherwise = do
    putStrLn $ "[-] " ++ (show id) ++ ". " ++ txt

displayToDo:: [Task] -> IO ()
displayToDo [] = putStrLn("")
displayToDo  ((id, done, txt) : ts) 
        | not(done) = do  display (id, done, txt)
        | otherwise = do displayToDo ts

displayDone:: [Task] -> IO ()
displayDone [] = putStrLn("")
displayDone  ((id, done, txt) : ts) 
        | done = do  display (id, done, txt)
        | otherwise = do displayDone ts


menu :: Todo -> IO ()
menu todo = do
    putStr("> ")
    line <- getLine
    
    case line of
        "print" -> displayAll todo
        "print todo" -> displayToDo (snd(todo) :: [Task])
        "print done" -> displayDone (snd(todo) :: [Task])  

        _ -> usage

main :: IO ()
main = do
    contents <- readFile "tasks.txt"
    let todo1 = read $! contents :: Todo
    --print todo1
    menu todo1
    --mapM_ print (snd todo1)